// vars/prodReleaseReport.groovy
import org.foo.Version
import org.foo.Constants
import groovy.xml.MarkupBuilder
import org.foo.Branches

def call(String ProdBranchName, appTag, sendTo) {
    jiraApiUrl = Constants.JIRA_API_URL
    bitbucketUrl = "${Constants.GITBASE.replace(':','/').replace('git@','https://') +'/'}"
    def gitReportData = []
    releaseVersionForEmail = ''

    appTag.each{ appName, newTag ->
        if (validateTag(newTag, appName) ){
            println "Tag ${newTag} found for ${appName}"
            println ("Finding previous tag for application " + appName)
            newTag = this.newTag
            oldTag = this.oldTag
            def branch = editProdBranch(ProdBranchName, "${appName}", this, false, true)
            println "Getting commits between tags ${newTag} and ${oldTag} for branch ${branch}"

            releaseVersionForEmail = getVersionForEmail(releaseVersionForEmail, newTag)

            currentDir = WORKSPACE
	    def newDir = "${currentDir}/${appName}"
	    dir(newDir){
                def gitCheckoutResult = git branch: "${branch}", url: "${Constants.GITBASE}/${appName}.git", credentialsId: "${Constants.GITSERVER_CREDENTIAL_DEFAULT}", poll: false
                def allCommits = sh (script: "git log ${oldTag}..${newTag} --pretty=%H", returnStdout: true).split("\r?\n")
                arrLen = Arrays.toString(allCommits)
                if (arrLen != '[]'){
                    for (commit in allCommits) {
                        def commitNum = commit        
                        eachCommit = [:]
                        //gitData = sh (script: "git log -1 ${commitNum} --pretty=%an,%f,%b,%cd --date=format:'%b-%d %Y %H:%M:%S'", returnStdout: true).trim()
                        gitData = sh (script: "git log -1 ${commitNum} --pretty=%an'==='%f'==='%b'==='%cd --date=local", returnStdout: true).trim()
                        if (!gitData.contains('release-build')){
                            def commitUrl = bitbucketUrl + appName + '/commits/' + commitNum
                            getPRJira = get_pr_jira(gitData.split('===')[1],jiraApiUrl,bitbucketUrl,appName)
                            eachCommit = ['Application':appName,
			                    'ReleaseVersion': newTag,
			                    'Commit':commitNum, 
                                            'CommitUrl':commitUrl, 
					    'Description':gitData.split('===')[1],
                                            'Commited By':gitData.split('===')[0], 
                                            'Commit Date':gitData.split('===')[3], 
                                            'Pull Request':getPRJira[0], 
                                            'PullRequestLink':getPRJira[1],
                                            'Jira':getPRJira[2],
                                            'jiraApiUrl':getPRJira[3], 
                                            'Approved By':gitData.split('===')[2]]
                            gitReportData = gitReportData + eachCommit
                        }
                    } 
                }
                deleteDir()
	    }
        }
    }
    if (gitReportData != []){ emailStatus = formatReportAndEmail(gitReportData,releaseVersionForEmail,sendTo) }
}

def validateTag(readTag,gitRepo) {
    //Check if tag is null
    if (readTag.trim().isEmpty()){ println "Empty tag provided."; return false }
    // Check if tag is numeric and defined pattern
    if ((!readTag.matches("\\d+\\.\\d+\\.\\d+")) && (readTag.toUpperCase() != 'LATEST')){ println "Tag is not properly formatted.";return false }
    //if Tag is Latest, then determine the current tag version
    if (readTag.equalsIgnoreCase('latest')) {
        branchesUtility = new Branches("${gitRepo}", this)
        result = branchesUtility.findHighestProdTag()
        if (!result) {
            println "Could not find any tags to process!"
        }
        readTag = result.toString()
    }
    //check if tag has a previous version
    if (decrementTag(readTag)){
        newTag = this.newTag
        oldTag = this.oldTag
        return [newTag,oldTag]
    }
    else { println "No previous tags found.";return false }
}

def decrementTag(String readNewTag) {
    def readOldTag = readNewTag
    try {
        Version newTagVersion = new Version(readNewTag,this)
        Version oldTagVersion = new Version(readOldTag,this)
        if ( (oldTagVersion.patch == 0) && (oldTagVersion.minor == 0)){
            oldTagVersion.decrementMajor()
        }
        else if (oldTagVersion.patch == 0){
            oldTagVersion.decrementMinor()
        }
        else {
            oldTagVersion.decrementPatch()
        }
	newTag = newTagVersion.toString()
	oldTag = oldTagVersion.toString()
        return [newTag, oldTag]
    }
    catch (e) {
        println "Error with the tag provided: ${e}"
        return false
        
    }
}

def getVersionForEmail(oldVer, newVer){
    origVerArr = [oldVer,newVer]
    convertToNumArr = []
    origVerArr.each { item ->
        if ( !item.trim().isEmpty()){
            convertToNumArr = convertToNumArr +  item.replaceAll("\\.","").toInteger()
        }
        else {convertToNumArr = convertToNumArr +  '0'}
    }
    index = convertToNumArr.indexOf(convertToNumArr.max())
    return origVerArr[index]
}

def get_pr_jira(gitString,jiraApiUrl,bitbucketUrl,appName){
    appRefTag = 'TMS'
    appName = appName.toUpperCase()
    pullReq = jiraNum = jLink = pullReqLink = ''
    try {
        if (gitString.contains('pull-request')){
            def extractData = gitString=~ /pull-request-(\d+)/
            pullReq = extractData[0][1]
            pullReqLink = bitbucketUrl + appName + '/pull-requests/' + pullReq
            if (gitString.toUpperCase().contains(appRefTag)){
                extractData = gitString.toUpperCase()=~ /${appRefTag}-(\d+)(.*)-PULL-REQUEST-(\d+)/
                jiraNum = extractData[0][1]
                jLink = jiraApiUrl + 'browse/' + appRefTag + '-' + jiraNum
            }
        }
    }
    catch (e) {
        println "Error trying to get Jira#: ${e}"
    }
    return [pullReq, pullReqLink, jiraNum, jLink]
}

@NonCPS
def get_approvedBy(allApprovals){
    def approvedBy = ''
    try {
        allApprovals =  allApprovals.split( '\n' ).findAll { it.contains( 'Approved-by' ) }
        allApprovals.unique().each{val ->
            val = val.replaceAll("Approved-by: ","")
            val = val.replaceAll('<', "(")
            val = val.replaceAll('>', ")")
            approvedBy = approvedBy + val + "<br />"
        }
    }
    catch (e) {
        approvedBy = "None"
    }
    return approvedBy
}

@NonCPS
def formatReportAndEmail(tasks, releaseVersion, sendTo) {
    reportColumns = ["Application", "ReleaseVersion", "Pull Request", "Jira", "Commit", "Commit Date", "Description", "Commited By", "Approved By"]
    def writer = new StringWriter()
    try {
        new MarkupBuilder(writer).table(border:1) {
            delegate.tr {
                reportColumns.each { title -> delegate.delegate.th(height:"50",title)}
            }
            for (eachTask in tasks){
                delegate.tr {
                    reportColumns.each { eachListItem ->
                        newVar = eachTask["${eachListItem}"]
                        if (eachListItem == "Commit"){
                            delegate.delegate.td{delegate.a(href:"${eachTask["CommitUrl"]}", newVar.take(6))}
                        }
                        else if (eachListItem == "Pull Request"){
                            delegate.delegate.td{delegate.a(href:"${eachTask["PullRequestLink"]}", newVar)}
                        }
                        else if (eachListItem == "Jira"){
                            delegate.delegate.td{delegate.a(href:"${eachTask["jiraApiUrl"]}", newVar)}
                        }
                        else if (eachListItem == "Approved By"){
                            approvedBy = get_approvedBy(newVar)
                            delegate.delegate.td{ mkp.yieldUnescaped approvedBy }
                        }
                        else { delegate.delegate.td(newVar) }
                    }
                }
            }
        }
        emailText =  writer.toString()

        emailext(subject: "Tasks in Production Release-${releaseVersion}" ,
        mimeType: 'text/html',
        body: "${emailText}",
        to: "${sendTo}",
        from: "shweta.agarwal@mercurygate.com"
        )
        emailStatus = "Success"
    }
    catch (e) {
    	println e
        emailStatus = "Failure"
    }
    return emailStatus
}
