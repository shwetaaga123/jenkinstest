import org.foo.Branches
import org.foo.Constants


/**
 * Edits the supplied branch name.
 *  If editProdBranch was not called from within a pipeline node{},
 *  then one is allocated for the remainder of processing.
 *
 *  1. if it's Groovy falsy or prod/LATEST, then:
 *       a. find the latest prod/... available.
 *       b. if updateDisplayName is true, then replace any
 *          occurrences of prod/LATEST in currentBuild.displayName
 *          with the resolved branch name.
 *       c. return the resolved branch name.
 *  2. if it's a prod/digit branch, then return it as-is.
 *  3. if requireProdType is false then return it as-is.
 *  4. if none of the above, then error.
 *
 * @param branch is either the branch name to be format-checked, or
 *               prod/LATEST (any case), or anything satisfying
 *               Groovy falsity.
 * @param gitRepo is either the name of the Git repository to look in, if
 *                the caller asked for the highest available prod/... branch,
 *                or it is an instance of the Branches class.
 * @param script in Jenkins using this class.
 *               May be omitted if and only if gitRepo is a
 *               Branches instance.
 * @param requireProdType whether or not the passed-in branch must
 *                        be prod/#.<whatever>.  If requireProdType
 *                        is false, then the only things editProdBranch
 *                        pays attention to are whether the passed-in
 *                        branch value satisfies Groovy truth and if it
 *                        matches Constants.DEFAULT_LATEST_PROD_VERSION
 *                        or not.
 * @param updateDisplayName if true (the default),  AND  if a branch
 *                          name was resolved (item 1, above),
 *                          then replace all occurrences of 'prod/LATEST'
 *                          in currentBuild.displayName with the resolved
 *                          branch name.
 *
 * @return the String value of branch, if it passes editing, or
 *         the highest prod/... branch found in the gitRepo, if
 *         branch is prod/LATEST or Groovy false.
 */
String call(def branch, def gitRepo, Script script=this, boolean requireProdType=true, boolean updateDisplayName=true) {
    String result = "** nothing happened yet **"
    if (env.NODE_NAME) {
        println("editProdBranch found active node ${env.NODE_NAME}.  Continuing with that.")
        result = doEdit(branch, gitRepo, script, requireProdType, updateDisplayName)
    } else {
        println("editProdBranch found no active node.")
        node() {
            println("\t... allocated ${env.NODE_NAME}")
            result = doEdit(branch, gitRepo, script, requireProdType, updateDisplayName)
        }
        println("\t... node deallocated.")
    }
    return result
}


/**
 * See Javadoc/Groovydoc for @{call} method.  All parameteters and returns must be identical between the two.
 */
private String doEdit(def branch, def gitRepo, Script script=this, boolean requireProdType=true, boolean updateDisplayName=true) {

    Branches branchesUtility = null
    def result = branch

    if (script == null  &&  ! gitRepo instanceof Branches) {
        error("script cannot be null when no Branches instance is provided")
    }

    if (gitRepo == null) {
        error("gitRepo cannot be null!")
    } else if (gitRepo instanceof Branches) {
        branchesUtility = gitRepo
        if (!script) {
            // If a Script was supplied by the caller or defaulted,
            // then use it.  If not, then get it from the supplied
            // instance of Branches.
            script = branchesUtility.script
        }
    }

    branch = getDefaultForFalsy(branch)
    if (branch.equalsIgnoreCase(Constants.DEFAULT_LATEST_PROD_VERSION)) {
        if (!branchesUtility) {
            branchesUtility = new Branches("$gitRepo", script)  // Branches constructor speifically requires a GString as first parameter
        }
        result = branchesUtility.findHighestProdBranch()
        if (!result) {
            error("Could not find prod/* branch to process!")
        }
        if (updateDisplayName) {
            currentBuild.displayName = currentBuild.displayName.replace(Constants.DEFAULT_LATEST_PROD_VERSION, result)
        }
    } else if (requireProdType  &&  !branch.matches(~/(?i)^${Constants.PROD_TYPE}\/\d+\..*$/)) {
        error("Branch must be ${Constants.PROD_TYPE}/<version> or omitted or ${Constants.DEFAULT_LATEST_PROD_VERSION} to find and use the latest - found '${branch}'")
    }
    script.println("Processing branch '${result}'")
    return result.toString()
}


/**
 * Resolves the branch input to the default value if is false accoring to "Groovy truth". This includes
 * false, null, "", 0.
 *
 * @param branch name input
 */
public String getDefaultForFalsy(def branch) {
    return (branch ?: Constants.DEFAULT_LATEST_PROD_VERSION)
}
