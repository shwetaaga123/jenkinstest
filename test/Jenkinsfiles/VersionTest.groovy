@Library('my-test-library@master') _   // Set this to the Git branch to be tested
import orig.foo.Version

DEBUG = true // Try putting this in the binding all the way out here, and see if the shared library can read it.

class XVersion extends Version {
    Object irrelevant;
    XVersion(Object x, Script s) {
        super(x, s)
    }
    XVersion(String x, Script s) {
        super(x, s)
    }
}

node () {
    int errors = 0, tests = 0
    Version a, b
    List<Version> unsortedList = [], sortedList = [], expectedList = []
    NavigableMap<Version> nmap = new TreeMap<>()

    stage('test constructors') {
        echo("\ntest constructors\n")

        tests++
        echo(sprintf("test %d:  test Version(String, Script) constructor\n", tests))
        a = new Version('1.2.3-SNAPSHOT',this)
        echo(sprintf("Expected 1.2.3-SNAPSHOT.  Got: '%s'\n", a.toString()))
        if (!a.toString().equals('1.2.3-SNAPSHOT')) {
            errors++
            echo('FAILED!\n')
        }

        tests++
        echo(sprintf("test %d:  test Version(Object, Script) constructor", tests))
        b = new Version(new StringBuilder("1.4.6"), this)
        echo(sprintf("Expected 1.4.6.  Got: '%s'\n", b.toString()))
        if (!b.toString().equals('1.4.6')) {
            errors++
            echo('FAILED!')
        }
    }
    stage('test updating Version objects') {
        echo("\ntest updating Version objects\n")
        a.major -= 2
        a.minor = 7
        a.patch++
        a.snapshot = false
        tests++
        echo(sprintf("test %d:  Expected -1.7.4.  Got: '%s'\n", tests, a.toString()))
        if (!a.toString().equals('-1.7.4')) {
            errors++
            echo('FAILED!')
        }

        b.patch -= 11
        b.incrementMajor()
        b.decrementMinor()
        tests++
        echo(sprintf("test %d:  Expected 2.3.-5.  Got: '%s'\n", tests, b.toString()))
        if (!b.toString().equals('2.3.-5')) {
            errors++
            echo('FAILED!')
        }
    }
    stage('test hashing Version objects') {
        echo("\ntest hashing Version objects\n")
        tests++
        echo(sprintf("test %d:  a='%s', hashCode %d,  b='%s', hashCode %d, should not match\n", tests, a.toString(), a.hashCode(), b.toString(), b.hashCode()))
        if (a.hashCode() == b.hashCode()) {
            errors++
            echo('FAILED!')
        }
        int aOldHash = a.hashCode()
        int bOldHash = b.hashCode()

        a.major = 3
        a.minor = 4
        a.patch = 5
        a.snapshot = true
        b.major = a.major
        b.minor = a.minor
        b.patch = a.patch
        b.snapshot = a.snapshot
        tests++
        echo(sprintf("test %d:  a='%s', hashCode %d,  b='%s', hashCode %d, should match\n", tests, a.toString(), a.hashCode(), b.toString(), b.hashCode()))
        if (a.hashCode() != b.hashCode()) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  Verify that a and b's hashCodes changed, when their values did\n", tests))
        echo(sprintf("a.hashCode() = %d, was %d;  b.hashCode() = %d, was %d\n",
               a.hashCode(), aOldHash, b.hashCode(), bOldHash))
        if (a.hashCode() == aOldHash  ||  b.hashCode() == bOldHash) {
            errors++
            echo('FAILED!')
        }
    }
    stage('test comparing Version objects') {
        echo("\ntest comparing Version objects\n")

        XVersion x = new XVersion(a,this)

        tests++
        echo(sprintf("test %d:  a='%s' should equal b='%s', Got %b\n", tests, a.toString(), b.toString(), a.equals(b)))
        if (a.equals(b) != true) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  a.compareTo(b) should be zero.  Got %d\n", tests, a.compareTo(b)))
        if (a.compareTo(b) != 0) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  a='%s' should equal x='%s', Got %b\n", tests, a.toString(), x.toString(), a.equals(x)))
        if (a.equals(x) != true) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  a.compareTo(x) should be zero.  Got %d\n", tests, a.compareTo(x)))
        if (a.compareTo(x) != 0) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  x='%s' should equal b='%s', Got %b\n", tests, x.toString(), b.toString(), x.equals(b)))
        if (x.equals(b) != true) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  x.compareTo(b) should be zero.  Got %d\n", tests, x.compareTo(b)))
        if (x.compareTo(b) != 0) {
            errors++
            echo('FAILED!')
        }

        a = new Version('0.9.9',this)
        b = new Version('1.5.9',this)
        tests++
        echo(sprintf("test %d:  a='%s' should be less than b='%s', i.e. a.compareTo(b) should be negative.  Got %d\n",
               tests, a.toString(), b.toString(), a.compareTo(b)))
        if (a.compareTo(b) > -1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and b.compareTo(a) should be positive.  Got %d\n", tests, b.compareTo(a)))
        if (b.compareTo(a) < 1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and a should not equal b.  a.equals(b) got %b\n", tests, a.equals(b)))
        if (a.equals(b) != false) {
            errors++
            echo('FAILED!')
        }

        a = new Version('1.5.9',this)
        b = new Version('1.5.010',this)
        tests++
        echo(sprintf("test %d:  a='%s' should be less than b='%s', i.e. a.compareTo(b) should be negative.  Got %d\n",
               tests, a.toString(), b.toString(), a.compareTo(b)))
        if (a.compareTo(b) > -1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and b.compareTo(a) should be positive.  Got %d\n", tests, b.compareTo(a)))
        if (b.compareTo(a) < 1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and a should not equal b.  a.equals(b) got %b\n", tests, a.equals(b)))
        if (a.equals(b) != false) {
            errors++
            echo('FAILED!')
        }

        a = new Version('1.4.9',this)
        b = new Version('1.5.9-SNAPSHOT',this)
        tests++
        echo(sprintf("test %d:  a='%s' should be less than b='%s', i.e. a.compareTo(b) should be negative.  Got %d\n",
               tests, a.toString(), b.toString(), a.compareTo(b)))
        if (a.compareTo(b) > -1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and b.compareTo(a) should be positive.  Got %d\n", tests, b.compareTo(a)))
        if (b.compareTo(a) < 1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and a should not equal b.  a.equals(b) got %b\n", tests, a.equals(b)))
        if (a.equals(b) != false) {
            errors++
            echo('FAILED!')
        }

        a = new Version('1.5.8',this)
        b = new Version('1.5.9',this)
        tests++
        echo(sprintf("test %d:  a='%s' should be less than b='%s', i.e. a.compareTo(b) should be negative.  Got %d\n",
               tests, a.toString(), b.toString(), a.compareTo(b)))
        if (a.compareTo(b) > -1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and b.compareTo(a) should be positive.  Got %d\n", tests, b.compareTo(a)))
        if (b.compareTo(a) < 1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and a should not equal b.  a.equals(b) got %b\n", tests, a.equals(b)))
        if (a.equals(b) != false) {
            errors++
            echo('FAILED!')
        }

        a = new Version('1.5.9-SNAPSHOT',this)
        b = new Version('1.5.9',this)
        tests++
        echo(sprintf("test %d:  a='%s' should be less than b='%s', i.e. a.compareTo(b) should be negative.  Got %d\n",
               tests, a.toString(), b.toString(), a.compareTo(b)))
        if (a.compareTo(b) > -1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and b.compareTo(a) should be positive.  Got %d\n", tests, b.compareTo(a)))
        if (b.compareTo(a) < 1) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("... %d ... and a should not equal b.  a.equals(b) got %b\n", tests, a.equals(b)))
        if (a.equals(b) != false) {
            errors++
            echo('FAILED!')
        }
    }
    stage('test a List<Version>') {
        echo("\ntest a List<Version>\n")

        tests++
        try {
            echo(sprintf("test %d:  load a list with out-of-order Versions", tests))
            unsortedList = [
                new Version('1.80.0',this),
                new Version('1.8.3-SNAPSHOT',this),
                new Version('0.80.0',this),
                new Version('01.80.0',this),
                new Version('5.0.0-SNAPSHOT',this),
            ]
            expectedList = [
                new Version('0.80.0',this),
                new Version('1.8.3-SNAPSHOT',this),
                // these two should be indistinguishable when read back out
                new Version('1.80.0',this),
                new Version('01.80.0',this),
                //
                new Version('5.0.0-SNAPSHOT',this),
            ].withEagerDefault{ null }  // prevent index-out-of-range by returning null
        } catch (org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException e) {
            echo("RestrictedAccessException caught:  after it is rethrown, please use the Manage Jenkins > Script Approval page to authorize it, and retry.")
            // If you don't let Jenkins catch RestrictedAccessException, then it won't
            // show up on the Script Approval page, so you can never cure the access
            // exception.
            throw e
        } catch (Exception e) {
            StringWriter s = new StringWriter()
            PrintWriter  p = new PrintWriter(s)
            p.println(e.message)
            e.printStackTrace(p)
            echo(s.toString())
            errors++
            echo('FAILED!')
        }
        tests++
        echo(sprintf("... %d ... expected 5 elements, got %d", tests, unsortedList.size()))
        if (unsortedList.size() != 5) {
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  sort the list", tests))
        sortedList.addAll(unsortedList)
        Collections.sort(sortedList)
        echo("... unsorted:")
        unsortedList?.eachWithIndex{ v, i ->
            echo(sprintf("    %d:  %s", i, v.toString()))
        }
        boolean sortedListFailed = false
        echo("... sorted:")
        sortedList.eachWithIndex{ v, i ->
            echo(sprintf("    %d:  expected:  %s,  got:  %s", i, expectedList[i], v))
            if ( ! expectedList[i]?.equals(v) ) {
                sortedListFailed = true
            }
        }
        if (expectedList.size() != sortedList.size()) {
            echo(sprintf("... expected %d elements, got %d", expectedList.size(), sortedList.size()))
            sortedListFailed = true
        }
        if (sortedListFailed) {
            errors++
            echo('FAILED!')
        }
    }
    stage('test a NavigableMap<Version>') {
        echo("\ntest a NavigableMap<Version>\n")

        tests++
        try {
            echo(sprintf("test %d:  insert unsorted Versions into the map", tests))
            // should get only one of '1.80.0' and '01.80.0', as duplicate keys
            unsortedList.each{
                nmap << [(it): it.toString()]
            }
        } catch (org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException e) {
            echo("RestrictedAccessException caught:  after it is rethrown, please use the Manage Jenkins > Script Approval page to authorize it, and retry.")
            // If you don't let Jenkins catch RestrictedAccessException, then it won't
            // show up on the Script Approval page, so you can never cure the access
            // exception.
            throw e
        } catch (Exception e) {
            StringWriter s = new StringWriter()
            PrintWriter  p = new PrintWriter(s)
            p.println(e.message)
            e.printStackTrace(p)
            echo(s.toString())
            errors++
            echo('FAILED!')
        }

        tests++
        echo(sprintf("test %d:  verify the map reads out in expected order", tests))
        boolean navigableMapFailed = false
        List expectedUniqueList = expectedList.toUnique()
        nmap.eachWithIndex{ k, v, i ->
            echo(sprintf("    %d:  expected:  %s,  got:  %s : %s", i, expectedUniqueList[i], k, v))
            if ( ! expectedUniqueList[i]?.equals(k) ) {
                navigableMapFailed = true
            }
        }
        if (expectedUniqueList.size() != nmap.size()) {
            echo(sprintf("... expected %d elements, got %d", expectedUniqueList.size(), nmap.size()))
            navigableMapFailed = true
        }
        if (navigableMapFailed) {
            errors++
            echo('FAILED!')
        }


    }
    stage('Summary') {
        echo(sprintf("\n %d out of %d tests failed.\n", errors, tests))
        if (errors) {
            throw new RuntimeException(String.format("%d out of %d tests failed", errors, tests))
        }
    }

}

return this
