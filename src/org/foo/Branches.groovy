//src/org/foo/Branches.groovy
package org.foo

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Class used to manage git branches
 */
class Branches implements Serializable {

    boolean debugFlag = false
    GString repository = null
    Script script
    GString url = null
    // All of these Maps are immutable (see getNewRefsMap())
    Map prevBranchMap = [:]
    Map prevTagMap    = [:]
    Map currBranchMap = [:]
    Map currTagMap    = [:]
    // LS_REMOTE_PARSER Matcher[0] groups
    //   $0 = a row of 'ls remote' output
    //   $1 = SHA1
    //   $2 = whether object is a branch head ("heads") or a tag ("tags")
    //   $3 = name of the branch or tag (we do not support names with embedded whitespace)
    private static final Pattern LS_REMOTE_PARSER = ~/\s*([a-fA-F0-9]++)\s++refs\/(\w++)\/(\S++)\s*/
    private static final Pattern LINEBREAKS = ~/\R/

    /**
     * Constructor
     *
     * @param repository in Git server containing the branches
     * @param script in jenkins using this class
     */
    Branches(GString repository, Script script) {

        if (repository == null) {
            error("repository cannot be null!")
        }

        if (script == null) {
            error("script cannot be null!")
        }

        this.repository = repository
        this.script = script
        this.url = "${Constants.GITBASE}/${repository}.git"
    }

    /**
     * Constructor
     *
     * @param repository in Git server containing the branches
     * @param script in jenkins using this class
     * @param debug flag
     */
    Branches(def repository, Script script, boolean debug=false) {

        if (repository == null) {
            error("repository cannot be null!")
        }

        if (script == null) {
            error("script cannot be null!")
        }

        this.debugFlag = debug
        this.repository = "${repository.toString()}"
        this.script = script
        this.url = "${Constants.GITBASE}/${repository}.git"
    }

    /**
     * Checks if remote branch exists in repository
     *
     * @param branch to check for existence
     * @return true if found; false otherwise
     */
    boolean branchExists(def branch) {

        GString command = "git ls-remote -h -q ${url}"
        log("branchExists.queryCommand: ${command}")

        def result = script.sh (script: "${command}", returnStdout: true).trim()
        def found = result.split('\n').any{
            if (it.endsWith("refs/heads/${branch}")) {
                log("branchExists.found: ${it}")
                return true
            }
        }
        return found
    }

    /**
     * Creates the hotfix branch for a repository if it doesn't already exists
     * otherwise raise an error
     *
     * @param version of the hotfix
     * @param dryRun when true will only simulate creation
     */
    void createHotfixBranch(def version, boolean dryRun) {

        GString hotfixBranch = "hotfix/${version}"
        if (!branchExists(hotfixBranch)) {

            checkout("${Constants.MASTER_BRANCH}")

            GString command = "git checkout -b ${hotfixBranch} ${Constants.MASTER_BRANCH}"
            log("checkout.command: ${command}")
            script.sh (script: "${command}", returnStdout: false)

            push(hotfixBranch, dryRun)

        } else {
            error("${hotfixBranch} branch already exists!")
        }
    }

    /**
     * Creates a maintenance branch (prod/*) for a repository if it doesn't already exist
     * otherwise raise an error
     *
     * @param version of the prod
     * @param dryRun when true will only simulate creation
     *
     * @return the prod branch's name, if it was created
     */
    String createProdBranch(Version version, boolean dryRun) {

        GString prodBranch = "${Constants.PROD_TYPE}/${version.major}.${version.minor}"
        if (!branchExists(prodBranch)) {

            checkout("${Constants.DEVELOP_BRANCH}")

            GString command = "git checkout -b ${prodBranch} ${Constants.DEVELOP_BRANCH}"
            log("checkout.command: ${command}")
            script.sh (script: "${command}", returnStdout: false)

            push(prodBranch, dryRun)

            return prodBranch.toString()

        } else {
            error("${prodBranch} branch already exists!")
        }

        return '***Error!  Should never get here!  Please report Branches class logic error***'
    }

    /**
     * Checkout branch
     *
     * @param branch to checkout
     */
    void checkout(def branch) {

        GString command = "git checkout ${branch}; git branch --set-upstream-to origin/${branch}"
        log("checkout.command: ${command}")

        script.sh (script: "${command}", returnStdout: false)

        logHead()
    }

    /**
     * Find the branch in the repository based on the type.
     * Assumes there can be at most one branch of the requested type.
     *
     * @param branchType can be either hotfix or release
     * @return branch name if found; otherwise null
     */
    GString findGitFlowBranch(def branchType) {

        GString branch = null
        GString command = "git ls-remote -h -q ${url}"
        log("findGitFlowBranch.command: ${command}")

        def result = script.sh (script: "${command}", returnStdout: true).trim()
        result.split('\n').findAll{
            if (it.contains("refs/heads/${branchType}/")) {
                String[] parts = it.split("refs/heads/${branchType}/")
                if (parts.length == 2) {
                    branch = "${branchType}/${parts[1]}"
                    log("findGitFlowBranch.found: ${branch}")
                    return
                }
            }
        }
        return branch
    }

    /**
     * Find the gitflow hotfix branch
     *
     * @return branch name if found; otherwise null
     */
    GString findHotfixBranch() {
        return findGitFlowBranch("${Constants.HOTFIX_TYPE}")
    }

    /**
     * Find the production branch with the highest version
     *
     * @return branch name if found; otherwise null
     */
    GString findHighestProdBranch() {
        String branch = null
        def command = "git ls-remote -h -q $url | "                          + // list remote branches in format commit-hash<tab>refs/heads/...
                      "cut -f2 | "                                           + // only want the "refs/heads/..." string
                      "grep -P -e '^refs/heads/${Constants.PROD_TYPE}/' | "  + // select the production branch lines
                      "cut -d/ -f4- | "                                      + // only want the the version # after "refs/heads/prod/"
                      "sort --sort=version --reverse"                          // sort by numeric dotted version numbers, highest-first
        log("findHighestProdBranch.command: $command")

        def result = script.sh (script: command, returnStdout: true).trim()
        log("findHighestProdBranch.result:\n$result")
        branch = "${Constants.PROD_TYPE}/${result.split("\n")[0]}".toString()  // force variable evaluation in our current context
        log("findHighestProdBranch.branch: $branch")
        return "$branch"
    }

     /**
     * Find the production tag with the highest version
     *
     * @return tag if found; otherwise null
     */
    GString findHighestProdTag() {
        String tag = null
        def command = "git ls-remote --tags --refs -q $url | "               + // list remote branches in format commit-hash<tab>refs/tags/...
                      "cut -f2 | "                                           + // only want the "refs/heads/..." string
                      "cut -d/ -f3- | "                                      + // only want the tag number after "refs/tags/"
                      "grep -P -e '^\\d+(\\.\\d+){2}\$' | "                  + // select only numeric tags that have format 1.1.1
                      "sort --sort=version --reverse"                          // sort by numeric dotted version numbers, highest-first
        log("findHighestProdTag.command: $command")

        def result = script.sh (script: command, returnStdout: true).trim()
        log("findHighestProdTag.result:\n$result")
        tag = "${result.split("\n")[0]}".toString()
        log("findHighestProdTag.Tag: $tag")
        return "$tag"
    }


    /**
     * Find the gitflow release branch
     *
     * @return branch name if found; otherwise null
     */
    GString findReleaseBranch() {
        return findGitFlowBranch("${Constants.RELEASE_TYPE}")
    }


    /**
     * Returns the previous (immutable) Map of branch names to HEAD
     * SHA1's for this Branch object's repo.
     *
     * ATTENTION!  Be careful of the order in which you call the
     *             get*BranchMap() methods, because getNewBranchMap()
     *             will drop prevBranchMap, move currBranchMap into
     *             prevBranchMap, and create a new currBranchMap.
     *
     * @return whatever the second-most-recently created immutable 
     *         Map<branch name, SHA1 of the branch_s HEAD>.
     *         Returns an empty Map, if only zero or one
     *         such Map exists.
     */
    Map getPrevBranchMap() {
        return prevBranchMap
    }

    /**
     * Returns the previous (immutable) Map of tag names to SHA1's
     * for this Branch object's repo.
     *
     * ATTENTION!  Be careful of the order in which you call the
     *             get*TagMap() methods, because getNewTagMap()
     *             will drop prevTagMap, move currTagMap into
     *             prevTagMap, and create a new currTagMap.
     *
     * @return whatever the second-most-recently created immutable 
     *         Map<tag name, SHA1 of the tag>.
     *         Returns an empty Map, if only zero or one
     *         such Maps exist.
     */
    Map getPrevTagMap() {
        return prevTagMap
    }

    /**
     * Returns the current (immutable) Map of branch names to HEAD
     * SHA1's for this Branch object's repo.
     *
     * ATTENTION!  Be careful of the order in which you call the
     *             get*BranchMap() methods, because getNewBranchMap()
     *             will drop prevBranchMap, move currBranchMap into
     *             prevBranchMap, and create a new currBranchMap.
     *
     * @see getNewRefsMap(def refType) for additional information on
     *      the characteristics of the returned Map.
     *
     * @return whatever is the most-ecently created immutable 
     *         Map<branch name, SHA1 of the branch_s HEAD>.
     *         Returns an empty Map, if no such Map exists.
     */
    Map getCurrBranchMap() {
        return currBranchMap
    }

    /**
     * Returns the current (immutable) Map of tag names to SHA1's
     * for this Branch object's repo.
     *
     * ATTENTION!  Be careful of the order in which you call the
     *             get*TagMap() methods, because getNewTagMap()
     *             will drop prevTagMap, move currTagMap into
     *             prevTagMap, and create a new currTagMap.
     *
     * @see getNewRefsMap(def refType) for additional information on
     *      the characteristics of the returned Map.
     *
     * @return whatever is the most-ecently created immutable 
     *         Map<tag name, SHA1 of the tag>.
     *         Returns an empty Map, if no such Map exists.
     */
    Map getCurrTagMap() {
        return currTagMap
    }

    /**
     * Creates a new immutable Map of branch names to HEAD SHA1's for
     * this Branch object's repo, and returns it.  It moves the original
     * currBranchMap to prevBranchMap.  It makes the just-created Map
     * immutable, and stores it in currBranchMap, before returning.
     *
     *
     * @see getNewRefsMap(def refType) for additional information on
     *      the characteristics of the returned Map.
     *
     * @return the just-created immutable
     *         Map<branch name, branch HEAD SHA1>.
     */
    Map getNewBranchMap() {
        prevBranchMap = currBranchMap
        currBranchMap = getNewRefsMap(Constants.LS_REMOTE_BRANCH_TYPE)
        return currBranchMap
    }

    /**
     * Creates a new immutable Map of tag names to their SHA1's for
     * this Branch object's repo, and returns it.  It moves the original
     * currTagMap to prevTagMap, and stores the new one as currTagMap.
     *
     * @see getNewRefsMap(def refType) for additional information on
     *      the characteristics of the returned Map.
     *
     * @return the just-created immutable
     *         Map<tag name, tag SHA1>.
     */
    Map getNewTagMap() {
        prevTagMap = currTagMap
        currTagMap = getNewRefsMap(Constants.LS_REMOTE_TAG_TYPE)
        return currTagMap
    }

    /**
     * Creates a new Map of branch or tag names to HEAD SHA1's for this
     * Branch object's repo, and returns it as an immutable Map.
     *
     * The returned Map will be sorted  --  alphabetically for non-
     * numeric parts of the branch or tag name, and version number
     * order for at least the first dotted.numeric.part of the name.
     * Currently, the Map returned is implemented with LinkedHashMap,
     * but that is not guaranteed to always be so (TODO:  try to find a
     * way to use something which implements SortedMap or NavigableMap,
     * but that would require a compareTo() method which duplicated the
     * Linux sort utility's -V option).
     *
     * WARNING!
     * The exact limits of the sorting technique (Linux sort utility's
     * -V  (sort in version number order, where possible) aren't
     * documented anyplace I could find, but it's been tested with
     * our maintenance branch implementations more common cases:
     *   +  version number tags work (e.g. 3.7.8 come before 3.7.19)
     *      GOTCHA:  a version number tag with an alphabetic suffix
     *      sorts after the same version number without a suffix
     *      e.g.  3.9.0-release-start comes after 3.9.0 .
     *   +  versioned branch names work (e.g. prod/1.4 prod/1.11)
     *   +  leading zeros work correctly (e.g.
     *      prod/1.3, prod/1.004, prod/01.9, prod/1.11)
     *   +  random non-version-number strings which may contain
     *      numbers.  E.G.
     *      'dmw-test-dev-apr-5-2017' sorts before
     *      'dmw-test-dev-apr-13-2015', which sorts before
     *      'dmw-test-dev-apr-17-2017', which sorts before
     *      'dmw-test-dev-feb-9-2016', which sorts before
     *      'dmw-test-dev-jun-4-2013'.
     *   +  m-d-y dates are NOT sorted in date sequence.
     *
     *  branch map notes:
     *      None at this time.
     *
     *  tag map notes:
     *      Git returns two entries for each tag:
     *          + the tag name, mapped to the SHA1 hash of the tag object
     *          + the tag name suffixed with '^{}' (shift-6, open- and
     *            close curly braces), mapped to the SHA1 hash of the
     *            commit which the tag points to.  As far as I can find,
     *            'git log' will always display the commit that the tag
     *            points to, regardless of which tag or SHA1 you use.
     *
     * @return the just-created immutable Map
     */
    Map getNewRefsMap(def refType) {
        Map result
        StringWriter s = new StringWriter(16*1024)
        GString command = "git ls-remote -q ${url} | sort -V -k2"
        log("getNewRefsMap.command: '$command', refType: '$refType'")
        try {
            String datalinesString = script.sh (script: "${command}", returnStdout: true).trim()
            if (debugFlag) {
                s.println("getNewRefsMap.debug: datalinesString is a ${datalinesString.getClass().name}; datalinesString=$datalinesString")
            }
            final int LENGTH_OF_SHORTEST_POSSIBLE_REFS_LINE = 56
            final int MAKE_SURE_MAP_ISNT_SILLY_SMALL        =  6
            Map<String,String> linesmap = new LinkedHashMap<>(datalinesString.length().intdiv(LENGTH_OF_SHORTEST_POSSIBLE_REFS_LINE) + MAKE_SURE_MAP_ISNT_SILLY_SMALL)
            // Silly Pipelines screws up Groovy code parsing so that datalinesString.eachLine{...} is only executed once.
            String[] datalinesArray = LINEBREAKS.split(datalinesString)
            s.println("getNewRefsMap.debug:  datalinesArray has ${datalinesArray.length} lines")
            for (String it in datalinesArray) {
                if ( it =~ LS_REMOTE_PARSER ) {
                    Matcher m = Matcher.lastMatcher
                    def linematched = m[0][0]
                    def linesha1    = m[0][1]
                    def linetype    = m[0][2]
                    def lineobject  = m[0][3]
                    if (debugFlag) {
                        s.println("line parsed:  $linematched\n\tSHA1 = $linesha1 \ttype = $linetype \tobject = $lineobject")
                    }
                    if ( linetype == refType ) {
                        linesmap[lineobject] = linesha1
                        if (debugFlag) {
                            s.println("$refType object mapped:  $lineobject: $linesha1")
                        }
                    }
                } else {
                    if (debugFlag) {
                        s.println("line did not match parsing:  $it")
                    }
                }
            }
            result = linesmap.asImmutable()
        } finally {
            try {
                // Pipelines doesn't support calling mapUtils.dumpMap() from within a library class, so ...
                s.print("getNewRefsMap returning:  (${result?.getClass()?.name})")
                if (result) {
                    s.println("[")
                    for (Map.Entry me in result) {
                        s.println("\t${me.key}\t :\t (${me.value.getClass().name}) '${me.value}'")
                    }
                    s.println("\t\t\t  ]")
                } else {
                    s.println("")
                }
            } finally {
                log(s.toString())
            }
        }
        return result
    }

    /**
     * prints the local changes to the branches
     */
    void logChanges() {

        String command = "git log --name-status @\\{u\\}.."

        def result = script.sh (script: "${command}", returnStdout: true).trim()
        log("changes: ${result}")
    }

    /**
     * prints the last commit to the branch
     */
    void logHead() {

        String command = "git log --name-status HEAD^..HEAD"

        def result = script.sh (script: "${command}", returnStdout: true).trim()
        log("last-commit: ${result}")
    }

    /**
     * Merge a branch into current branch
     *
     * @param branch to merge into local branch that is checkout
     * @param message to record for merge
     */
    void merge(def branch, def message) {

        GString command
        if (message == null) {
            command = "git merge --no-ff ${branch} -m \"pipeline merge ${branch}\""
        } else {
            command = "git merge --no-ff ${branch} -m \"${message}\""
        }
        log("merge.command: ${command}")

        script.sh(script: "${command}", returnStdout: false)
    }

    /**
     * Merge a branch into another branch
     *
     * Upon normal return, the mergeIntoBranch will be the current branch.
     *
     * @param mergeFromBranch is the source branch to merge from
     * @param mergeIntoBranch is the target branch to merge into
     * @param message to record for merge
     */
    void merge(def mergeFromBranch, def mergeIntoBranch, def message) {

        checkout(mergeFromBranch)
        checkout(mergeIntoBranch)
        merge(mergeFromBranch, message)
    }

    /**
     * Perform a dummy merge
     *
     * Upon normal return, the mergeIntoBranch will be the current branch.
     *
     * @param mergeFromBranch is the source branch to merge from
     * @param mergeIntoBranch is the target branch to merge into
     * @param message to record for merge
     */
    void mergeDummy(def mergeFromBranch, def mergeIntoBranch, def message) {

        checkout(mergeFromBranch)
        checkout(mergeIntoBranch)

        GString command = "git merge -s ours ${mergeFromBranch} -m \"${message}\""
        log("mergeDummy.command: ${command}")

        script.sh(script: "${command}", returnStdout: false)
    }

    /**
     * Push local changes for checkout branch to remote
     *
     * @param branchOrTag is the name of branch or tag
     * @param dryRun when true will only simulate push
     */
    void push(def branchOrTag, boolean dryRun) {

        GString command = "git push origin ${branchOrTag}"
        if (dryRun) {
            command = "git push --dry-run origin ${branchOrTag}"
        }
        log("push.command: ${command}")

        script.sh(script: "${command}", returnStdout: false)
    }

    /**
     * Push all local changes (with or without tags) to remote
     *
     * @param dryRun when true will only simulate push
     * @param doTags when true will also push tags.  Default is false (for compatibility with existing code).
     */
    void pushAll(boolean dryRun, boolean doTags=false) {

        String command = "git push --all"
        if (dryRun) {
            command = "git push --dry-run --all"
        }
        log("push.command: ${command}")

        script.sh(script: "${command}", returnStdout: false)

        if (doTags) {
            pushTags(dryRun)
        }
    }

    /**
     * Push only local tag changes to remote
     *
     * @param dryRun when true will only simulate push
     */
    void pushTags(boolean dryRun) {

        String command = "git push --tags"
        if (dryRun) {
            command = "git push --dry-run --tags"
        }
        log("push.command: ${command}")

        script.sh(script: "${command}", returnStdout: false)
    }

    /**
     * Create a local tag for checkout branch
     *
     * @param version to used as tag name
     * @param message to record for tag reason
     * @param commit to tag (optional. defaults to HEAD)
     */
    void tag(def version, def message, def commit="") {

        GString command = "git tag -a ${version} -m '${message}' ${commit}"
        log("tag.command: ${command}")

        script.sh(script: "${command}", returnStdout: false)
    }

    /**
     * Check if tag already exists
     *
     * @param tag to find
     * @return true if tag is found; false otherwise
     */
    boolean tagExists(def tag) {

        GString command = "git ls-remote -t --refs -q ${url}"
        log("tagExists.queryCommand: ${command}")

        def result = script.sh (script: "${command}", returnStdout: true).trim()
        def found = result.split('\n').any{
            if (it.endsWith("refs/tags/${tag}")) {
                log("tagExists.found: ${it}")
                return true
            }
        }
        return found
    }

    private void log(def msg) {
        script.echo("Branches.${msg}")
    }
}
