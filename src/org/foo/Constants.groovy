package org.foo

class Constants {

    public static final String GITBASE = 'git@bitbucket.org:shwetaaga123'
    public static final String JIRA_API_URL = 'https://mercurygate.atlassian.net/'
    public static final String GITSERVER_CREDENTIAL_BITBUCKET = 'bitbucketSSH'
    public static final String GITSERVER_CREDENTIAL_DEFAULT = GITSERVER_CREDENTIAL_BITBUCKET
    public static final String PROD_TYPE = 'prod'
    public static final String DEFAULT_LATEST_PROD_VERSION = PROD_TYPE + "/LATEST"

}
