//src/org/foo/Version.groovy
package org.foo

/**
 * Class used to work with version
 */
class Version implements Serializable, Comparable<? extends Version> {

    private static final int EMPTY_INTEGER = 0

    Script script
    int major = EMPTY_INTEGER
    int minor = EMPTY_INTEGER
    int patch = EMPTY_INTEGER
    boolean snapshot = false

    Version(String version, Script script) {
        this(version, script, 'dummy')
        script.echo('Version String constructor.  Original input object was a ')
        script.echo("\t\t" + version?.getClass()?.name)
    }

    Version(Object version, Script script) {
        this(version, script, 'dummy')
    }

    /**
     * The 3-arg constructor holds all the common constructor code.
     *
     * @param version   Something to get version info from.
     * @param script    A Script which provides the context for logging.
     * @param dummy     Does nothing.  This parameter is always ignored.
     *                  Its sole purpose is to have a signature which
     *                  cannot match Version(String,Script), so the
     *                  String constructor won't call itself in an
     *                  infinite loop.
     */
    private Version(Object version, Script script, def dummy) {

        if (script == null) {
            error("script cannot be null!")
        }
        this.script = script
        script.echo('Version constructor.  Input object is now a ')
        script.echo("\t\t" + version?.getClass()?.name)

        if (version == null) {
            error("version cannot be null!")
        }

        String versionString = version?.toString()
        script.echo("version arg (as String): ${versionString}")

        if (version instanceof Version) {
            script.echo('Version copying from a Version.')
            this.major = version.major
            this.minor = version.minor
            this.patch = version.patch
            this.snapshot = version.snapshot
            // Every field in a Version object must be represented here!
            return
        }

        List items = versionString.split("\\.")
        script.echo("version split parts: ${items}")

        if (items[-1].endsWith('-SNAPSHOT')) {
            this.snapshot = true
            items[-1] = items[-1] - '-SNAPSHOT'
        }

        this.major = checkItem(items,0,'major')
        this.minor = checkItem(items,1,'minor')
        this.patch = checkItem(items,2,'patch')

        if ((items.size() < 1) || (items.size() > 3)) {
            error("invalid version (must be in format major[.minor[.patch]][-SNAPSHOT])")
        }
    }

    @NonCPS
    private static int checkItem(List<String> items, int idx, String term) {
        String s
        if (idx < 0) {
            throw new IllegalArgumentException("Version.checkItem called with negative index " + Integer.toString(idx))
        }
        if (items.size() > idx) {
            s = items.get(idx)
            if (!s.isInteger()) {
                // Hopefully, this will produce a stack trace, not just a failure message:
                throw new IllegalArgumentException("Found invalid "+term+" version '"+s+"'.  Version string must be in the form major[.minor[.patch]][-SNAPSHOT], where major, minor, and patch are integers.")
            }
            return s.toInteger().intValue()
        } else {
            return EMPTY_INTEGER
        }
    }

    @NonCPS
    void decrementMajor() {this.major--}

    @NonCPS
    void decrementMinor() {this.minor--}

    @NonCPS
    void decrementPatch() {this.patch--}

    @NonCPS
    void incrementMajor() {this.major++}

    @NonCPS
    void incrementMinor() {this.minor++}
    
    @NonCPS
    void incrementMinor(int incrBy) {this.minor = this.minor + incrBy}

    @NonCPS
    void incrementPatch() {this.patch++}
    
    @NonCPS
    String getBaseString() {
        return "${this.major}.${this.minor}.${this.patch}".toString()
    }

    @NonCPS
    @Override
    String toString() {
        return "${this.major}.${this.minor}.${this.patch}${snapshot ? '-SNAPSHOT' : ''}".toString()
    }

    @NonCPS
    @Override
    int hashCode() {
        final int prime = 31
        int result = 1
        result = prime * result + Integer.valueOf(major).hashCode()
        result = prime * result + Integer.valueOf(minor).hashCode()
        result = prime * result + Integer.valueOf(patch).hashCode()
        result = prime * result + Boolean.valueOf(snapshot).hashCode()
        return result
    }

    @NonCPS
    @Override
    boolean equals(Object obj) {
        if (obj == null) {
            return false
        } else if (obj.is(this)) {
            return true
        } else if (obj instanceof Version) {
            Version other = (Version) obj
            return ( major == other.major  &&  minor == other.minor  &&  patch == other.patch  &&  snapshot == other.snapshot )
        }
        return false
    }

    @NonCPS
    @Override
    int compareTo(Version other) {
        int comp = 0;
        if (other == null) {
            throw new NullPointerException("Version.compareTo(other) found other==null");
        }
        comp = Integer.compare(this.major,other.major);
        if (comp != 0) {
            return comp;
        }
        comp = Integer.compare(this.minor,other.minor);
        if (comp != 0) {
            return comp;
        }
        comp = Integer.compare(this.patch,other.patch);
        if (comp != 0) {
            return comp;
        }
        // A -SNAPSHOT version (snapshot == true) is LOWER THAN
        // a Release version (snapshot == false) ==> that
        // true < false, so must reverse the sense of the compare.
        comp = Boolean.compare(other.snapshot,this.snapshot);
        return comp;
    }

}
